from django import forms
from django.forms import widgets
from lab_1.models import Friend
# https://www.geeksforgeeks.org/django-modelform-create-form-from-models/

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
        widgets = {'dob': forms.DateInput(attrs={'type':'date'}),}

    

    

