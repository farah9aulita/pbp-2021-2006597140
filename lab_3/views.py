from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.http import  HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    friends = Friend.objects.all().values()  
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST or None)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3/')

    else:
        form = FriendForm()

    response = {'form':form}

    return render(request, 'lab3_form.html', response )

