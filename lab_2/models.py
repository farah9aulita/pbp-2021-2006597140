from django.db import models

class Note(models.Model):
    recipient = models.CharField("To", max_length=30)
    sender = models.CharField("From",max_length=30)
    title = models.CharField("Title",max_length=30)
    message = models.TextField("Message",max_length=300)

    def __str__(self):
        return self.title
