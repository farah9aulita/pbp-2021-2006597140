from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField('NPM', max_length= 10)
    dob = models.DateField('Date of Birth')
    # TODO Implement missing attributes in Friend model

    def __str__(self):
        return self.name
